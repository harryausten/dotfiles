####################
# PROGRAM DEFAULTS #
####################

set_program_variable()
{
    var="$1"
    shift

    while (( $# )); do
        if test -x "$(which "$1" 2> /dev/null)"; then
            eval "export $var=$1"
            return
        fi
        shift
    done
}

# Browser
set_program_variable BROWSER qutebrowser firefox
# Editor
set_program_variable EDITOR nvim vim vi
# Terminal file explorer
set_program_variable FILE ranger nautilus dolphin
# Pager
set_program_variable PAGER less more
# Terminal emulator
set_program_variable TERMINAL alacritty termite gnome-terminal
# PDF Viewer
set_program_variable VIEWER zathura evince
# Window Manager
set_program_variable WM sway i3

############
# ENV VARS #
############

# Default TERM variable setting
if [ -z "${TERM:-}" ]; then
    export TERM='xterm-256color'
fi

# Qt application display settings (use QT5)
export QT_QPA_PLATFORMTHEME="qt5ct"

# Reduce number of XKB warnings
export XKB_LOG_LEVEL="critical"

# Add local bin tools to path
if ! echo "$PATH" | grep -q "$HOME/.local/bin"; then
    export PATH="$HOME/.local/bin:$PATH"
fi

# Where to download youtube videos using personal script
export YOUTUBE_DOWNLOAD_LOCATION="$HOME/vidz"

# Set GTK theme environment variable
export GTK_THEME="$(grep 'gtk-theme-name' "$HOME/.config/gtk-3.0/settings.ini" | sed 's/.*=//')"

# Move less history file out of home dir
export LESSHISTFILE="$HOME/.cache/less/history"

# Java GUI application workaround for Wayland window managers
export _JAVA_AWT_WM_NONREPARENTING=1

# Vivado env (are SuSE libs okay? Seems to work...)
export XILINX_VIVADO='/opt/Xilinx/Vivado/2022.1'
#export LD_LIBRARY_PATH="$XILINX_VIVADO/lib/lnx64.o/SuSE"
export PATH="$PATH:$XILINX_VIVADO/bin"

# Ath10k tools
export PATH="$PATH:$HOME/repos/qca-swiss-army-knife/tools/scripts/ath10k"

# Full rust program backtraces by default
export RUST_BACKTRACE=1
# Local cargo-installed tools (search first)
export PATH="$HOME/.cargo/bin:$PATH"

# Disable incremental builds on Android/termux to avoid failed hard-linking warnings
if [ "$(uname -o)" = Android ]; then
    export CARGO_INCREMENTAL=0
fi

# Point perf to XDG_CONFIG_DIR rather than top level home
export PERF_CONFIG="$HOME/.config/perfconfig"

#########################
# WAYLAND SPECIFIC VARS #
#########################

if [ "$WM" = sway ]; then
    # Set XDG session to wayland
    export XDG_SESSION_TYPE="wayland"

    # Set XDG desktop to sway
    export XDG_CURRENT_DESKTOP="sway"

    # Remove borders from QT windows in wayland
    export QT_WAYLAND_DISABLE_WINDOWDECORATION=1

    # Tell firefox to use wayland instead of xwayland
    export MOZ_ENABLE_WAYLAND=1
fi

#################
# PROCESS FILES #
#################

# Source bashrc if running bash
if echo "$0" | grep -q "bash$"; then
    if test -r ~/.bashrc; then
        . "$HOME/.bashrc"
    fi
fi

# Source less colours
if test -r "$HOME/.config/less-termcap"; then
    . "$HOME/.config/less-termcap"
fi

# Install crontabs
if command -v crontab >/dev/null 2>&1; then
    if test -r "$HOME/.config/cron/user"; then
        crontab "$HOME/.config/cron/user" > /dev/null
    fi

    if test -r "$HOME/.config/cron/root"; then
        sudo crontab "$HOME/.config/cron/root" > /dev/null
    fi
fi

# Ensure local binary programs are up to date
if test -r "$HOME/.config/startup.mk"; then
    make -f "$HOME/.config/startup.mk" > /dev/null 2>&1
fi

#############
# LAUNCH WM #
#############

# If logged on to tty1 and the window manager is
# not already running, run it
if [ "$(tty)" = "/dev/tty1" ]; then
    if ! pgrep -x "$WM" > /dev/null; then
        exec "$WM" &> "$HOME/.cache/$WM.log"
    fi
fi
