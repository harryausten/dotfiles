/*
 * Header for kb-input program
 * Defines all charactters and respective keycodes
 * Note: '£' and '¬' are currently not handled as they are wide characters (2 chars)
 */

#include <linux/uinput.h>

#define NUM_KEYS 50
const int KEYCODES[NUM_KEYS] = {
    KEY_1, KEY_2, KEY_3, KEY_4, KEY_5, KEY_6, KEY_7, KEY_8, KEY_9,
    KEY_0, KEY_MINUS, KEY_EQUAL, KEY_Q, KEY_W, KEY_E, KEY_R,
    KEY_T, KEY_Y, KEY_U, KEY_I, KEY_O, KEY_P, KEY_LEFTBRACE,
    KEY_RIGHTBRACE, KEY_A, KEY_S, KEY_D, KEY_F, KEY_G, KEY_H,
    KEY_J, KEY_K, KEY_L, KEY_SEMICOLON, KEY_APOSTROPHE, KEY_GRAVE,
    KEY_LEFTSHIFT, KEY_BACKSLASH, KEY_102ND, KEY_Z, KEY_X, KEY_C,
    KEY_V, KEY_B, KEY_N, KEY_M, KEY_COMMA, KEY_DOT, KEY_SLASH,
    KEY_SPACE
};

#define NUM_NORMAL_KEYS 49
const char NORMAL_KEYS[NUM_NORMAL_KEYS] = {
    ' ', '#', '\'', ',', '-', '.', '/', '0', '1', '2', '3', '4',
    '5', '6', '7', '8', '9', ';', '=', '[', '\\', ']', '`', 'a',
    'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm',
    'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y',
    'z'
};

const int NORMAL_KEYCODES[NUM_NORMAL_KEYS] = {
    KEY_SPACE, KEY_BACKSLASH, KEY_APOSTROPHE, KEY_COMMA, KEY_MINUS,
    KEY_DOT, KEY_SLASH, KEY_0, KEY_1, KEY_2, KEY_3, KEY_4, KEY_5,
    KEY_6, KEY_7, KEY_8, KEY_9, KEY_SEMICOLON, KEY_EQUAL,
    KEY_LEFTBRACE, KEY_102ND, KEY_RIGHTBRACE, KEY_GRAVE, KEY_A,
    KEY_B, KEY_C, KEY_D, KEY_E, KEY_F, KEY_G, KEY_H, KEY_I, KEY_J,
    KEY_K, KEY_L, KEY_M, KEY_N, KEY_O, KEY_P, KEY_Q, KEY_R, KEY_S,
    KEY_T, KEY_U, KEY_V, KEY_W, KEY_X, KEY_Y, KEY_Z
};

#define NUM_SHIFTED_KEYS 46
const char SHIFTED_KEYS[NUM_SHIFTED_KEYS] = {
    '!', '"', '$', '%', '&', '(', ')', '*', '+', ':', '<', '>',
    '?', '@', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J',
    'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V',
    'W', 'X', 'Y', 'Z', '^', '_', '{', '|', '}', '~'
};

const int SHIFTED_KEYCODES[NUM_SHIFTED_KEYS] = {
    KEY_1, KEY_2, KEY_4, KEY_5, KEY_7, KEY_9, KEY_0, KEY_8, KEY_EQUAL,
    KEY_SEMICOLON, KEY_COMMA, KEY_DOT, KEY_SLASH, KEY_APOSTROPHE,
    KEY_A, KEY_B, KEY_C, KEY_D, KEY_E, KEY_F, KEY_G, KEY_H, KEY_I,
    KEY_J, KEY_K, KEY_L, KEY_M, KEY_N, KEY_O, KEY_P, KEY_Q, KEY_R,
    KEY_S, KEY_T, KEY_U, KEY_V, KEY_W, KEY_X, KEY_Y, KEY_Z, KEY_6,
    KEY_MINUS, KEY_LEFTBRACE, KEY_102ND, KEY_RIGHTBRACE, KEY_BACKSLASH
};

/* Struct representing the virtual keyboard */
struct keyboard
{
    int fd;
    char prev_char;
};
