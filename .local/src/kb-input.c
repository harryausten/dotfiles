/*
 * kb-input.c
 * C program to simulate keystrokes
 * Useful for automating typing
 */

/* Local includes */
#include "kb-input.h"
/* System includes */
#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

/* Wrapper macro for errno error CHECK */
#define CHECK(X) if (X == -1) { fprintf( stderr, "ERROR (%s:%d) -- %s\n", __FILE__, __LINE__, strerror(errno) ); exit(-1); }
extern int errno;

/* Character pointer comparer for use with bsearch */
int cmp_chars( const void * a, const void * b )
{
    return ( *(char*)a - *(char*)b );
}

/* Set valid key/event codes */
void set_keys( int fd )
{
    /* Enable key events */
    CHECK( ioctl( fd, UI_SET_EVBIT, EV_KEY ) );

    /* Enable all valid keys */
    for ( int i = 0; i != NUM_KEYS; ++i )
    {
        CHECK( ioctl( fd, UI_SET_KEYBIT, KEYCODES[i] ) );
    }
}

/* Emit a single uinput event */
void emit( int fd, int type, int code, int val )
{
    struct input_event ie;

    ie.type = type;
    ie.code = code;
    ie.value = val;
    /* timestamp values below are ignored */
    ie.time.tv_sec = 0;
    ie.time.tv_usec = 0;

    CHECK( write( fd, &ie, sizeof(ie) ) );

    /* Allow processing time for uinput before sending next event */
    usleep( 50 );
}

/* Single key event and report */
void emit_key( int fd, int code, int val )
{
    emit( fd, EV_KEY, code, val );
    emit( fd, EV_SYN, SYN_REPORT, 0 );
}

/* Simulate a quick key press */
void press_key( int fd, int code )
{
    /* send press */
    emit_key( fd, code, 1 );
    /* send release */
    emit_key( fd, code, 0 );
}

/* Simulate shifted key press (for uppercase/specials) */
void press_shifted_key( int fd, int code )
{
    /* send shift press */
    emit_key( fd, KEY_LEFTSHIFT, 1 );
    /* simulate keypress */
    press_key( fd, code );
    /* send shift release */
    emit_key( fd, KEY_LEFTSHIFT, 0 );
}

/* Simulate entering a given character */
void enter_char( struct keyboard * k, int c )
{
    /*
     * TODO: Add in the following wide characters somehow:
     * Char -62:-93 = £
     * Char -62:-84 = ¬
     */
    void * found;
    void * begin_norm = (void *)&NORMAL_KEYS;
    void * begin_shift = (void *)&SHIFTED_KEYS;

    if (( found = bsearch( &c, begin_norm, NUM_NORMAL_KEYS, sizeof(char), cmp_chars ) ))
    {
        int index = ( (char *)found - (char *)begin_norm ) / sizeof(char);
        press_key( k->fd, NORMAL_KEYCODES[index] );
    }
    else if (( found = bsearch( &c, begin_shift, NUM_SHIFTED_KEYS, sizeof(char), cmp_chars ) ))
    {
        int index = ( (char *)found - (char *)begin_shift ) / sizeof(char);
        press_shifted_key( k->fd, SHIFTED_KEYCODES[index] );
    }
    /* Ignore new line characters */
    else if ( c != '\n' )
    {
        fprintf( stderr, "Unsupported character (%d:%c) cannot be entered!\n", c, c );
    }

    k->prev_char = c;
}

int main( int argc, char ** argv )
{
    /* Should only have one argument */
    if ( argc > 2 )
    {
        fprintf( stderr, "Usage:\t%s <string>\n\tpipe <string> | %s\n", argv[0], argv[0] );
        exit( -1 );
    }

    /* Setup virtual keyboard */
    struct keyboard virt_keyboard;
    memset( &virt_keyboard, 0, sizeof(virt_keyboard) );
    virt_keyboard.prev_char = '\0';
    {
        struct uinput_setup usetup;
        CHECK( ( virt_keyboard.fd = open( "/dev/uinput", O_WRONLY | O_NONBLOCK ) ) );

        /* Events/Keys setup */
        set_keys( virt_keyboard.fd );

        /* uinput device setup */
        memset( &usetup, 0, sizeof(usetup) );
        usetup.id.bustype = BUS_USB;
        usetup.id.vendor = 0x1234; /* sample vendor */
        usetup.id.product = 0x5678; /* sample product */
        strcpy( usetup.name, "KB-Input Virtual Keyboard" );

        CHECK( ioctl( virt_keyboard.fd, UI_DEV_SETUP, &usetup ) );
        CHECK( ioctl( virt_keyboard.fd, UI_DEV_CREATE ) );
    }

    /* Give userspace time to detect new device */
    sleep( 1 );

    if ( argc == 2 )
    {
        /* Take input from input argument */
        for ( int i = 0; argv[1][i] != '\0'; ++i )
        {
            enter_char( &virt_keyboard, argv[1][i] );
        }
    }
    else
    {
        /* Take input from stdin */
        int c;
        while ( ( c = getchar() ) != EOF )
        {
            enter_char( &virt_keyboard, c );
        }
    }

    /* Give userspace time to read events */
    sleep( 1 );

    /* Destroy virtual keyboard */
    CHECK( ioctl( virt_keyboard.fd, UI_DEV_DESTROY ) );
    CHECK( close( virt_keyboard.fd ) );

    return 0;
}
