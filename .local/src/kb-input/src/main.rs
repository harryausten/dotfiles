use mouse_keyboard_input::{key_codes::*, Error, VirtualDevice};
use std::collections::HashMap;
use std::io::Read;
use std::thread;
use std::time::Duration;

const DELAY: Duration = Duration::from_secs(1);

fn press_key(dev: &mut VirtualDevice, key: u16) -> Result<(), Error> {
    dev.press(key)?;
    dev.release(key)?;
    Ok(())
}

fn press_shifted_key(dev: &mut VirtualDevice, key: u16) -> Result<(), Error> {
    dev.press(KEY_LEFTSHIFT)?;
    press_key(dev, key)?;
    dev.release(KEY_LEFTSHIFT)?;
    Ok(())
}

const NORMAL_KEYS: [(u8, u16); 49] = [
    (b' ', KEY_SPACE),
    (b'#', KEY_BACKSLASH),
    (b'\'', KEY_APOSTROPHE),
    (b',', KEY_COMMA),
    (b'-', KEY_MINUS),
    (b'.', KEY_DOT),
    (b'/', KEY_SLASH),
    (b'0', KEY_10),
    (b'1', KEY_1),
    (b'2', KEY_2),
    (b'3', KEY_3),
    (b'4', KEY_4),
    (b'5', KEY_5),
    (b'6', KEY_6),
    (b'7', KEY_7),
    (b'8', KEY_8),
    (b'9', KEY_9),
    (b';', KEY_SEMICOLON),
    (b'=', KEY_EQUAL),
    (b'[', KEY_LEFTBRACE),
    (b'\\', KEY_102ND),
    (b']', KEY_RIGHTBRACE),
    (b'`', KEY_GRAVE),
    (b'a', KEY_A),
    (b'b', KEY_B),
    (b'c', KEY_C),
    (b'd', KEY_D),
    (b'e', KEY_E),
    (b'f', KEY_F),
    (b'g', KEY_G),
    (b'h', KEY_H),
    (b'i', KEY_I),
    (b'j', KEY_J),
    (b'k', KEY_K),
    (b'l', KEY_L),
    (b'm', KEY_M),
    (b'n', KEY_N),
    (b'o', KEY_O),
    (b'p', KEY_P),
    (b'q', KEY_Q),
    (b'r', KEY_R),
    (b's', KEY_S),
    (b't', KEY_T),
    (b'u', KEY_U),
    (b'v', KEY_V),
    (b'w', KEY_W),
    (b'x', KEY_X),
    (b'y', KEY_Y),
    (b'z', KEY_Z),
];

const SHIFTED_KEYS: [(u8, u16); 46] = [
    (b'!', KEY_1),
    (b'"', KEY_2),
    (b'$', KEY_4),
    (b'%', KEY_5),
    (b'&', KEY_7),
    (b'(', KEY_9),
    (b')', KEY_10),
    (b'*', KEY_8),
    (b'+', KEY_EQUAL),
    (b':', KEY_SEMICOLON),
    (b'<', KEY_COMMA),
    (b'>', KEY_DOT),
    (b'?', KEY_SLASH),
    (b'@', KEY_APOSTROPHE),
    (b'A', KEY_A),
    (b'B', KEY_B),
    (b'C', KEY_C),
    (b'D', KEY_D),
    (b'E', KEY_E),
    (b'F', KEY_F),
    (b'G', KEY_G),
    (b'H', KEY_H),
    (b'I', KEY_I),
    (b'J', KEY_J),
    (b'K', KEY_K),
    (b'L', KEY_L),
    (b'M', KEY_M),
    (b'N', KEY_N),
    (b'O', KEY_O),
    (b'P', KEY_P),
    (b'Q', KEY_Q),
    (b'R', KEY_R),
    (b'S', KEY_S),
    (b'T', KEY_T),
    (b'U', KEY_U),
    (b'V', KEY_V),
    (b'W', KEY_W),
    (b'X', KEY_X),
    (b'Y', KEY_Y),
    (b'Z', KEY_Z),
    (b'^', KEY_6),
    (b'_', KEY_MINUS),
    (b'{', KEY_LEFTBRACE),
    (b'|', KEY_102ND),
    (b'}', KEY_RIGHTBRACE),
    (b'~', KEY_BACKSLASH),
];

fn main() -> Result<(), Error> {
    let mut dev = VirtualDevice::new();

    // Give userspace time to detect new device
    thread::sleep(DELAY);

    let normal_keys = HashMap::from(NORMAL_KEYS);
    let shifted_keys = HashMap::from(SHIFTED_KEYS);

    // Process stdin and emit chars to `dev`
    let mut stdin = std::io::stdin().lock().bytes();
    while let Some(Ok(byte)) = stdin.next() {
        if let Some(key) = normal_keys.get(&byte) {
            press_key(&mut dev, *key)?;
        } else if let Some(key) = shifted_keys.get(&byte) {
            press_shifted_key(&mut dev, *key)?;
        } else {
            eprintln!("Unsupported character ({}) cannot be entered!\n", byte);
        }
    }

    // Give userspace time to read events
    thread::sleep(DELAY);
    Ok(())
}
