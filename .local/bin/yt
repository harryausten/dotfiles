#!/bin/sh -eu
# Script to download youtube videos

# Parse options
quality=''
name=''
while getopts an:q: opt; do
    case "$opt" in
        a)
            quality="audio"
            ;;
        n)
            name="$OPTARG"
            ;;
        q)
            quality="$OPTARG"
            ;;
        ?)
            echo "Usage: $0 [-a] [-q quality] [-n name] [url]"
            exit 1
            ;;
    esac
done
shift $((OPTIND - 1))

# Get video URL
if [ "$#" -gt 1 ]; then
    echo "Usage: $0 [-a] [-q quality] [-n name] [url]"
    exit 1
elif [ "$#" -eq 1 ]; then
    url="$1"
    shift
else
    printf 'Enter URL: '
    read -r url
fi

# Remove any query parameters from url following & symbols
url="${url%%&*}"

# Get name
if [ -z "$name" ]; then
    printf "Video name: "
    read -r name
    echo ""
fi

# If audio quality option provided, download audio only
if [ "$quality" = audio ]; then
    youtube-dl "$url" -f bestaudio -o "${YOUTUBE_DOWNLOAD_LOCATION:-.}/${name:-%(title)s}.%(ext)s"
    exit 0
fi

# Get video/audio formats
echo "Getting video/audio formats..."
fmt="$(youtube-dl -F "$url")"

# If quality not specified, list all available resolutions and let user decide
if [ -z "$quality" ]; then
    options="$(echo "$fmt" | awk '{print $4}' | sed 's/^[^0-9].*//g;/^$/d' | uniq)"
    echo "$options"
    echo ""
    printf "Enter quality: "
    read -r quality
fi

found=0

while [ $found -eq 0 ]; do
    # Attempt to select best video format of specified quality
    options="$(echo "$fmt" | grep -w "$quality")"
    if [ -n "$quality" ] && [ -n "$options" ]; then
        video="$(echo "$options" | sort -nk11 | sed -n 1p | awk '{print $1}')"
        echo "Quality \"$quality\" found!"
        echo ""
        found=1
    else
        echo "Quality \"$quality\" not found!"
        echo ""
        options="$(echo "$fmt" | awk '{print $4}' | sed 's/^[^0-9].*//g;/^$/d' | uniq)"
        echo "$options"
        echo ""
        printf "Enter quality: "
        read -r quality
    fi
done

# Set env var YOUTUBE_DOWNLOAD_LOCATION to desired download directory
# e.g. "$HOME/vidz" for desktop
# or "$HOME/storage/downloads" for mobile
echo "Downloading video in $quality..."
youtube-dl "$url" -f "$video"+bestaudio -o "${YOUTUBE_DOWNLOAD_LOCATION:-.}/${name:-%(title)s}.%(ext)s"
