#!/bin/sh -eu
# $1 - kernel version
# $2 - kernel image file (relative to top of linux repo)
# $3 - kernel map file
# $4 - install path

KERNEL="$2"
MAP="$3"
INSTALL_DIR="$4"
KERNEL_INSTALL="$INSTALL_DIR/$(basename "$KERNEL")"
MAP_INSTALL="$INSTALL_DIR/$(basename "$MAP")"
BACKUP_DIR="$INSTALL_DIR/prev/"
DTB_DIR="$(dirname "$KERNEL")/dts/"
DTB=""
HOSTNAME="$(hostname)"

case "$HOSTNAME" in
	Harry-PinebookPro)
		DTB='rockchip/rk3399-pinebook-pro.dtb'
		;;
	archiso|Harry-LinuxPC)
		KERNEL_INSTALL="$INSTALL_DIR/vmlinuz"
		;;
	*)
		echo "Running on an unknown machine ($HOSTNAME)!"
		exit 1
		;;
esac

# Ensure backup directory exists
install -vdm 755 "$BACKUP_DIR"

# Install kernel
if [ -f "$KERNEL_INSTALL" ]; then
	mv -v "$KERNEL_INSTALL" "$BACKUP_DIR"
fi
install -vm 644 "$KERNEL" "$KERNEL_INSTALL"

# Install devicetree (if exists)
if [ -n "$DTB" ]; then
	DTB_INSTALL="$INSTALL_DIR/$(basename "$DTB")"
	if [ -f "$DTB_INSTALL" ]; then
		mv -v "$DTB_INSTALL" "$BACKUP_DIR"
	fi
	install -vm 644 "${DTB_DIR}${DTB}" "$DTB_INSTALL"
fi

# Install system map
if [ -f "$MAP_INSTALL" ]; then
	mv -v "$MAP_INSTALL" "$BACKUP_DIR"
fi
install -vm 644 "$MAP" "$MAP_INSTALL"
