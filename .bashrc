# Exit if not running interactively
# $- contains the current bash option flags
[[ $- != *i* ]] && return

###############
# COMPLETIONS #
###############

# Enable bash completion
if test -r /usr/share/bash-completion/bash_completion; then
    source /usr/share/bash-completion/bash_completion
fi

# Rust tools bash completion
if command -v rustup >/dev/null 2>&1; then
	source <(rustup completions bash cargo)
	source <(rustup completions bash rustup)
fi

#################
# SHELL OPTIONS #
#################

# Update display when terminal is resized
shopt -s checkwinsize
# Aliases are expanded
shopt -s expand_aliases
# Enable history appending instead of overwriting
shopt -s histappend
# Correct minor directory misspelling
shopt -s cdspell
# Automatically cd into directories
shopt -s autocd
# Interpret ** as anything (multiple directories)
shopt -s globstar

#########################
# ENVIRONMENT VARIABLES #
#########################

# Change history file location to not clutter home dir
export HISTFILE="$HOME/.cache/bash/history"
# Ignore duplicates and leading spaces
export HISTCONTROL=ignoreboth
# Increase number of commands saved in history file (default 500)
export HISTSIZE=10000

# Colourful prompt
if tput setaf 1 > /dev/null 2>&1; then
    if [ "$(uname -o)" = Android ]; then
        export PS1="\[$(tput bold)\]\[$(tput setaf 5)\]\W \[$(tput setaf 7)\]\\$ \[$(tput sgr0)\]"
    else
        export PS1="\[$(tput bold)\]\[$(tput setaf 2)\]\u@\h \[$(tput setaf 5)\]\W \[$(tput setaf 7)\]\\$ \[$(tput sgr0)\]"
    fi
fi

###########
# ALIASES #
###########

# Source aliases
if test -r "$HOME/.config/aliasrc"; then
    source "$HOME/.config/aliasrc"
fi
