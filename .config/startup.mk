# Makefile ensuring programs are up to date on startup

# Directories
PREFIX  := $(HOME)/.local
SRC_DIR := $(PREFIX)/src/
INC_DIR := $(PREFIX)/inc/
BIN_DIR := $(PREFIX)/bin/
# GCC compiler flags
CFLAGS  := -Wall -Wextra -Wpedantic -O3 -I$(INC_DIR)

# Compile auto typing program
$(BIN_DIR)kb-input: $(SRC_DIR)kb-input.c
	$(CC) $(CFLAGS) $^ -o $@
