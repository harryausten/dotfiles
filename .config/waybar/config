{
    "layer": "bottom",
    "position": "top",
    "height": 30,

    "modules-left": ["sway/workspaces", "custom/disk", "custom/memory", "custom/cpu", "custom/temp"],
    "modules-center": ["sway/window"],
    "modules-right": ["tray", "custom/packages", "backlight", "pulseaudio", "network", "battery", "clock"],
    "sway/workspaces": {
        "format": "{name}"
    },
    "sway/window": {
        "max-length": 50,
        "tooltip": false
    },
    "clock": {
        "format": "📅 {:%a %d %b %T}",
        "interval": 1,
        "tooltip": false
    },
    "battery": {
        "format": "{icon} {capacity}",
        "format-icons": ["", "", "", "", ""],
        "format-charging": " {capacity}",
        "interval": 30,
        "states": {
            "warning": 25,
            "critical": 10
        },
        "tooltip": false
    },
    "network": {
        "format": "{icon} {essid} - {ipaddr}",
        "format-disconnected": "{icon}",
        "format-icons": {
            "wifi": [""],
            "ethernet": ["🖧"],
            "disconnected": ["❌"]
        },
        "on-click": "termite -e nmtui",
        "tooltip": false
    },
    "pulseaudio": {
        "format": "{icon} {volume}",
        "format-muted": "🔇",
        "format-icons": {
            "headphones": "🎧",
            "headset": "🎧",
            "default": ["🔈", "🔉", "🔊"]
        },
        "scroll-step": 10,
        "on-click": "pavucontrol",
        "tooltip": false
    },
    "backlight": {
        "format": "{icon} {percent}",
        "format-icons": ["🔅", "🔆"],
        "on-scroll-down": "light -A 1",
        "on-scroll-up": "light -U 1",
        "smooth-scrolling-threshold": 4
    },
    "tray": {
        "icon-size": 18
    },
    "custom/packages": {
        "format": "{}",
        "exec": "packages",
        "interval": "once",
        "on-click": "[ -n \"$(yay -Qu)\" ] && $TERMINAL -e upgrade",
        "signal": 1,
        "tooltip": false
    },
    "custom/disk": {
        "format": "{icon} {}",
        "format-icons": "🏠",
        "exec": "disk /home",
        "interval": 60,
        "tooltip": false
    },
    "custom/memory": {
        "format": "{icon} {}",
        "format-icons": "🧠",
        "exec": "free -h | awk '/^Mem:/ {print $3 \"/\" $2}'",
        "interval": 30,
        "tooltip": false
    },
    "custom/cpu": {
        "format": "{icon} {}",
        "format-icons": "💻",
        "exec": "cpu",
        "interval": 15,
        "tooltip": false
    },
    "custom/temp": {
        "format": "{icon} {percentage}°C",
        "format-icons": ["❄", "🔥"],
        "exec": "temp",
        "return-type": "json",
        "interval": 15,
        "tooltip": false
    }
}
