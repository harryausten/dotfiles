-- =======
-- PLUGINS
-- =======

-- Bootstrap packer
local ensure_packer = function()
	local fn = vim.fn
	local install_path = fn.stdpath('data') .. '/site/pack/packer/start/packer.nvim'
	if fn.isdirectory(install_path) == 0 then
		fn.system({
			'git',
			'clone',
			'--depth',
			'1',
			'https://github.com/wbthomason/packer.nvim',
			install_path})
		vim.cmd [[packadd packer.nvim]]
		return true
	end
	return false
end
local packer_bootstrap = ensure_packer()

-- Initialise packer
require('packer').startup({
	function()
		-- Allow packer to update itself
		use 'wbthomason/packer.nvim'
		-- Language Server Protocol support
		use 'neovim/nvim-lspconfig'
		use 'j-hui/fidget.nvim'
		-- Airline statusbar
		use 'vim-airline/vim-airline'
		-- Completion
		use 'hrsh7th/nvim-cmp'
		use 'hrsh7th/cmp-nvim-lsp'

		-- Automatically set up plugins after cloning packer
		if packer_bootstrap then
			require('packer').sync()
		end
	end
})

-- Completion
local cmp = require('cmp')
cmp.setup({
	snippet = {
		expand = function(args)
			vim.snippet.expand(args.body)
		end,
	},
	mapping = cmp.mapping.preset.insert({
		['<C-Space>'] = cmp.mapping.complete(),
		['<C-y>'] = cmp.mapping.confirm({ select = true }),
	}),
	sources = cmp.config.sources({
		{ name = 'nvim_lsp' },
	}, {
		{ name = 'buffer' },
	}),
})

-- Snippets
vim.keymap.set({ 'i', 's' }, '<C-j>',
	function() if vim.snippet.active({ direction = -1 }) then vim.snippet.jump(-1) end end)
vim.keymap.set({ 'i', 's' }, '<C-k>',
	function() if vim.snippet.active({ direction = 1 }) then vim.snippet.jump(1) end end)

-- Language Server Protocol
local lspconfig = require('lspconfig')
local on_attach = function(_, _)
	vim.keymap.set('n', 'gD', vim.lsp.buf.declaration, {buffer=0})
	vim.keymap.set('n', 'gd', vim.lsp.buf.definition, {buffer=0})
	vim.keymap.set('n', 'K', vim.lsp.buf.hover, {buffer=0})
	vim.keymap.set('n', 'gi', vim.lsp.buf.implementation, {buffer=0})
	vim.keymap.set('n', '<C-k>', vim.lsp.buf.signature_help, {buffer=0})
	vim.keymap.set('n', '<leader>D', vim.lsp.buf.type_definition, {buffer=0})
	vim.keymap.set('n', '<leader>r', vim.lsp.buf.rename, {buffer=0})
	vim.keymap.set('n', '<leader>a', vim.lsp.buf.code_action, {buffer=0})
	vim.keymap.set('n', 'gr', vim.lsp.buf.references, {buffer=0})
	vim.keymap.set('n', '<leader>e', vim.diagnostic.open_float, {buffer=0})
	vim.keymap.set({'n', 'v'}, '<leader>f', vim.lsp.buf.format, {buffer=0})
	vim.keymap.set(
		'n',
		'<leader>i',
		function()
			vim.lsp.inlay_hint.enable(not vim.lsp.inlay_hint.is_enabled({}))
		end,
		{ buffer = 0 }
	)
end
local capabilities = vim.tbl_deep_extend(
	'force',
	vim.lsp.protocol.make_client_capabilities(),
	require('cmp_nvim_lsp').default_capabilities()
)
-- C/C++
lspconfig.clangd.setup {
	on_attach = on_attach,
	capabilities = capabilities,
	cmd = {
		'clangd',
		'--header-insertion=never',
	},
}
-- Lua
lspconfig.lua_ls.setup{
	on_attach = on_attach,
	on_init = function(client)
		client.config.settings.Lua = vim.tbl_deep_extend('force', client.config.settings.Lua, {
			runtime = {version = 'LuaJIT'},
			workspace = {checkThirdParty = false, library = {vim.env.VIMRUNTIME}},
		})
	end,
	capabilities = capabilities,
	settings = {
		Lua = {
			diagnostics = { globals = { 'use', 'vim' } },
		}
	}
}
-- Rust
lspconfig.rust_analyzer.setup{
	on_attach = on_attach,
	capabilities = capabilities,
	settings = {
		['rust-analyzer'] = {
			check = {
				command = 'clippy',
			},
			procMacro = {
				enable = true,
			},
		},
	},
}
-- TOML
lspconfig.taplo.setup{on_attach = on_attach, capabilities = capabilities}
-- YAML
lspconfig.yamlls.setup{on_attach = on_attach, capabilities = capabilities}
-- Zig
lspconfig.zls.setup{on_attach = on_attach, capabilities = capabilities}
require('fidget').setup{}

-- =======
-- GENERAL
-- =======

-- Disable VI backwards compatibility
vim.opt.compatible = false

-- Auto-detect filetypes
vim.cmd('filetype plugin on')

-- Default to interpreting .tex files as LaTeX
vim.g.tex_flavor = 'latex'

-- Enable syntax highlighting
vim.cmd('syntax on')

-- =======
-- DISPLAY
-- =======

-- Show current and relative line numbers
vim.opt.number = true
vim.opt.relativenumber = true

-- Show cursor position in bottom right
vim.opt.ruler = true

-- Show the current command in bottom left
vim.opt.showcmd = true

-- Highlight matching brackets
vim.opt.showmatch = true

-- Only update display when needed (i.e. not during macro etc.)
vim.opt.lazyredraw = true

-- Search as characters are entered
vim.opt.incsearch = true

-- Don't highlight search
vim.opt.hlsearch = false

-- Sensible split behaviour
vim.opt.splitbelow = true
vim.opt.splitright = true

-- No banner in netrw
vim.g.netrw_banner = 0
-- Line numbers in netrw
vim.g.netrw_bufsettings = 'noma nomod nowrap number relativenumber ro nobl'

-- ================
-- TABS AND INDENTS
-- ================

-- Automatically use tabs/spaces
vim.opt.expandtab = false
vim.opt.smarttab = true

-- Tab length
vim.opt.tabstop = 8
vim.opt.softtabstop = 8
vim.opt.shiftwidth = 8

-- Wrap lines at 120 characters
vim.opt.textwidth = 120

-- Auto indentation
vim.opt.autoindent = true
vim.opt.smartindent = true

-- Allow all backspacing
vim.opt.bs = 'indent,eol,start'

-- No line wrap
vim.opt.wrap = false

-- =====
-- FILES
-- =====

-- Search down into subfolders
vim.opt.path:append '**'

-- Tab completion menu
vim.opt.wildmenu = true

-- Permanent undo
local home = os.getenv('HOME')
local undo_dir = home .. '/.config/nvim/undodir'
if vim.fn.isdirectory(undo_dir) == 0 then
    os.execute('install -m 700 ' .. undo_dir)
end
vim.opt.undodir = undo_dir
vim.opt.undofile = true

-- ============
-- KEY MAPPINGS
-- ============

-- Leader key for custom bindings
vim.g.mapleader = ' '

-- Move up/down visual lines
vim.api.nvim_set_keymap('n', 'j', 'gj', {noremap = true})
vim.api.nvim_set_keymap('n', 'k', 'gk', {noremap = true})

-- Toggle search highlighting
vim.api.nvim_set_keymap('n', '<C-h>', ':set invhlsearch<CR>', {noremap = true})

-- Allow mouse scrolling/clicking
vim.opt.mouse = 'a'

-- GIT commit messsage
vim.api.nvim_create_autocmd('VimEnter', {pattern = 'COMMIT_EDITMSG', command = 'startinsert!'})

-- BitBake include file syntax
vim.api.nvim_create_autocmd({'BufNewFile', 'BufRead'}, {pattern = '*.inc', command = 'set filetype=bitbake'})
-- Kernel build makefile file syntax
vim.api.nvim_create_autocmd({'BufNewFile', 'BufRead'}, {pattern = 'Kbuild', command = 'set filetype=make'})
-- UDEV rules syntax
vim.api.nvim_create_autocmd({'BufNewFile', 'BufRead'}, {pattern = '*.rules', command = 'set filetype=udevrules'})
-- Xilinx core instance file (IP core configuration)
vim.api.nvim_create_autocmd({'BufNewFile', 'BufRead'}, {pattern = '*.xci', command = 'set filetype=xml'})

-- ===========
-- Colorscheme
-- ===========

-- Base colorscheme
vim.cmd('colorscheme koehler')

-- Transparent background if using GUI
vim.api.nvim_set_hl(0, 'Normal', { bg = 'none' })

-- General symbol colouring
vim.api.nvim_set_hl(0, 'Identifier', { fg = 'white' })
vim.api.nvim_set_hl(0, 'Type', { fg = 'green' })
vim.api.nvim_set_hl(0, 'Function', { fg = 'cyan' })

-- Diff colours
vim.api.nvim_set_hl(0, 'DiffAdd', { fg = 'green' })
vim.api.nvim_set_hl(0, 'DiffChange', { fg = 'green' })
vim.api.nvim_set_hl(0, 'DiffDelete', { fg = 'red' })
vim.api.nvim_set_hl(0, 'DiffText', { fg = 'green' })

-- Somewhat readable floating windows
vim.api.nvim_set_hl(0, 'VertSplit', { fg = 'white' })
vim.diagnostic.config {
	float = { border = 'single', source = true }
}
require('lspconfig.ui.windows').default_options = {
	border = 'single'
}

-- Less distracting sign (LSP diagnostics) column
vim.api.nvim_set_hl(0, 'SignColumn', { bg = 'none' })

-- Somewhat readable popup menu
vim.api.nvim_set_hl(0, 'Pmenu', { fg = 'white' })
vim.api.nvim_set_hl(0, 'PmenuSel', { fg = 'cyan' })
