#!/usr/bin/nft -f
# vim:set ft=conf ts=2 sw=2 et:

# IPv4/IPv6 Simple & Safe firewall ruleset.
# More examples in /usr/share/nftables/ and /usr/share/doc/nftables/examples/.

table inet filter
delete table inet filter
table inet filter {
  chain input {
    type filter hook input priority filter
    policy drop

    # Connection tracker
    ct state invalid drop comment "early drop of invalid connections"
    ct state {established, related} accept comment "allow tracked connections"

    # Accept all loopback traffic
    iifname lo accept comment "allow from loopback"

    # Accept ICMP (e.g. pings)
    ip protocol icmp accept comment "allow icmp"
    meta l4proto ipv6-icmp accept comment "allow icmp v6"

    # Accept local multicast
    ip daddr 224.0.0.0/24 accept comment "allow local multicast"
    ip6 daddr ff02::/16 accept comment "allow link-local multicast"

    # Reject NetBIOS services
    tcp dport 135-139 drop comment "reject netbios tcp"
    udp dport 135-139 drop comment "reject netbios udp"

    # Reject Spotify device discovery
    tcp dport 57621 drop comment "reject spotify"
    udp dport 57621 drop comment "reject spotify"

    # Accept BOOTP/DHCP
    udp sport 67 udp dport 68 accept comment "allow dhcp client"
    udp sport 68 udp dport 67 accept comment "allow dhcp server"
    udp sport 547 udp dport 546 accept comment "allow dhcpv6 client"
    udp sport 546 udp dport 547 accept comment "allow dhcpv6 server"

    # Accept specific ports
    tcp dport 22 accept comment "allow ssh"
    udp dport 53 accept comment "allow dns"
    tcp dport 7777 accept comment "allow terraria"
    tcp dport 8096 accept comment "allow jellyfin"
    tcp dport 32400 accept comment "allow plex"

    # Reject broadcast
    meta pkttype broadcast drop comment "reject broadcast"

    # Count and log all remaining dropped packets
    counter
    log flags all prefix "DROP "
  }

  # Allow all forwarding
  chain forward {
    type filter hook forward priority filter
    policy accept
  }
}
