# Exit if not running interactively
# $- contains the current bash option flags
[[ $- != *i* ]] && return

##################
# Tab completion #
##################

# Basic auto/tab complete
autoload -U compinit
zstyle ':completion:*' menu select
zmodload zsh/complist
if ! test -d "$HOME/.cache/zsh"; then
    mkdir -p "$HOME/.cache/zsh"
fi
compinit -d "$HOME/.cache/zsh/zcompdump-$ZSH_VERSION"
_comp_options+=(globdots)

#################
# Shell options #
#################

# Enable history appending instead of overwriting
setopt appendhistory
# Automatically cd into directories
setopt autocd
# Regex globbing with #, ~ and ^
setopt extendedglob

# Vi mode
bindkey -v
export KEYTIMEOUT=1

#########################
# Environment variables #
#########################

# Enable colours and change prompt
autoload -U colors && colors
export PS1="%B%F{green}%n@%M %F{magenta}%~%f $%b "
export RPS1="%(?..%B%F{red}%?%f%b"

# Save history to cache directory to keep home dir clean
export HISTFILE="$HOME/.cache/zsh/history"

# Set history file length
export HISTSIZE=1000
export SAVEHIST=1000

# Source aliases
if test -r "$HOME/.config/aliasrc"; then
    source "$HOME/.config/aliasrc"
fi

# Load zsh syntax highlighting
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh 2> /dev/null
